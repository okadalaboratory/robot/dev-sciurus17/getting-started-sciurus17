# Getting started - Sciurus17

## 公式サイトの情報
[Sciurus17 研究用上半身人型ロボット](https://rt-net.jp/products/sciurus17/)

[入門ガイド](https://rt-net.jp/wp-content/uploads/2020/04/Sciurus17-Getting-Started-Guide-20210601.pdf)

[組み立てマニュアル](https://sites.google.com/view/sciurus17-assembly-manual/)


[サンプルコードのURL](https://github.com/rt-net/sciurus17_ros/blob/master/sciurus17_examples/README.md)

[ROS Wiki](http://wiki.ros.org/sciurus17)

[ハードウェア](https://github.com/rt-net/sciurus17_Hardware)

[ソフトウェア](https://github.com/rt-net/sciurus17_ros)



## 文献
[拡張制御バリア関数による Sciurus17 の機体保護制御](https://www.jstage.jst.go.jp/article/jacc/63/0/63_1211/_pdf)


## SNS
[Twitter](https://twitter.com/intent/follow?original_referer=https%3A%2F%2Frt-net.github.io%2F&ref_src=twsrc%5Etfw%7Ctwcamp%5Ebuttonembed%7Ctwterm%5Efollow%7Ctwgr%5Ert_corp&screen_name=rt_corp)
